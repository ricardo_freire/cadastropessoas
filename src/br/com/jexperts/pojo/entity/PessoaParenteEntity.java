package br.com.jexperts.pojo.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * Entidade que representa a tabela de relacionamento para mapear o parentesco entre usuarios.
 * 
 * @author Ricardo Davanço Freire
 *
 */

@Entity
@Table(name="pessoa_parente")
public class PessoaParenteEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_pessoa_parente")
	private Long idPessoaParente;
	
	@ManyToOne
	private PessoaEntity pessoa;
	
	@Column(name="id_parente")
	private PessoaEntity parente;
	
	@Column(name="grau_parentesco")
	private String grauParentesco;

	public Long getIdPessoaParente() {
		return idPessoaParente;
	}

	public void setIdPessoaParente(Long idPessoaParente) {
		this.idPessoaParente = idPessoaParente;
	}

	public PessoaEntity getPessoa() {
		return pessoa;
	}

	public void setPessoa(PessoaEntity pessoa) {
		this.pessoa = pessoa;
	}

	public PessoaEntity getParente() {
		return parente;
	}

	public void setParente(PessoaEntity parente) {
		this.parente = parente;
	}

	public String getGrauParentesco() {
		return grauParentesco;
	}

	public void setGrauParentesco(String grauParentesco) {
		this.grauParentesco = grauParentesco;
	}
	
	
	

}
