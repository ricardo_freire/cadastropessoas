package br.com.jexperts.pojo.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * 
 * Classe persistente relacionada à tabela pessoa.
 * 
 * @author Ricardo Davanço Freire
 *
 */

@Entity
@Table(name="pessoa")
public class PessoaEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_pessoa")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idPessoa;
	
	@Column(name="nome_completo")
	private String nomeCompleto;
	
	@Column(name="genero")
	private String genero;
	
	@Column(name="data_nascimento")
	private Date dataNascimento;
	
	@Column(name="cpf")
	private String cpf;
	
	@Column(name="municipio")
	private String municipio;
	
	@Column(name="uf")
	private String unidadeFederativa;
	
	@Column(name="tel_celular")
	private String telefoneCelular;
	
	@Column(name="tel_fixo")
	private String telefoneFixo;
	
	@Column(name="email")
	private String email;
	
	@Column(name="foto")
	private String foto;
	
	@Column(name="miniatura_foto")
	private String miniaturaFoto;
	
	//@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL, mappedBy="pessoa" )
	//private List<PessoaParenteEntity> pessoaParentes;

	public Long getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(Long idPessoa) {
		this.idPessoa = idPessoa;
	}

	public String getNomeCompleto() {
		return nomeCompleto;
	}

	public void setNomeCompleto(String nomeCompleto) {
		this.nomeCompleto = nomeCompleto;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getUnidadeFederativa() {
		return unidadeFederativa;
	}

	public void setUnidadeFederativa(String unidadeFederativa) {
		this.unidadeFederativa = unidadeFederativa;
	}

	public String getTelefoneCelular() {
		return telefoneCelular;
	}

	public void setTelefoneCelular(String telefoneCelular) {
		this.telefoneCelular = telefoneCelular;
	}

	public String getTelefoneFixo() {
		return telefoneFixo;
	}

	public void setTelefoneFixo(String telefoneFixo) {
		this.telefoneFixo = telefoneFixo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getMiniaturaFoto() {
		return miniaturaFoto;
	}

	public void setMiniaturaFoto(String miniaturaFoto) {
		this.miniaturaFoto = miniaturaFoto;
	}
	
	
	

}
