package br.com.jexperts.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.jexperts.pojo.entity.PessoaEntity;


/**
 * 
 * Última camada para acesso ao banco de dados.
 * Esta classe recebe as ingformações de <b>PessoaListaBusiness</b> e não deve ter regras de negócio, 
 * apenas comunicação com banco de dados.
 * 
 * @author Ricardo Davanço Freire
 *
 */
@Stateless
public class PessoaListaDAO {
	
	@PersistenceContext
	private EntityManager entityManager; 

	/**
	 * 
	 * Método que cria o jpql e faz a seleção na tabela <b>pessoa</b>.
	 * 
	 * @return Lista de <b>PessoaEntity</b> para ser processada no DataTable
	 */
	public List<PessoaEntity> selecionaPessoas() {
		String jpql = "SELECT p FROM PessoaEntity p";
		TypedQuery<PessoaEntity> query = entityManager.createQuery(jpql, PessoaEntity.class);
		return query.getResultList();
	}
	

}
