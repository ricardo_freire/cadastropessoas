package br.com.jexperts.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.jexperts.pojo.entity.PessoaEntity;

/**
 * 
 * Última camada para acesso ao banco de dados.
 * Esta classe não deve ter regras de negócio, apenas comunicação com banco de dados.
 * 
 * @author Ricardo Davanço Freire
 *
 */
@Stateless
public class PessoaDAO {
	
	@PersistenceContext
	EntityManager entityManger;
	
	/**
	 * Método que persiste a pessoa cadastrada no banco de dados.
	 * 
	 * @param pessoa - Entidade pronta para ser persistida na base de dados.
	 */
	public void salvar(PessoaEntity pessoa)
	{
		entityManger.persist(pessoa);
	}

}
