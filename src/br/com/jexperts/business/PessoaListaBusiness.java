package br.com.jexperts.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.jexperts.dao.PessoaListaDAO;
import br.com.jexperts.pojo.entity.PessoaEntity;


/**
 * 
 * Classe de business para as regras de negócio da tela de cadastro e edição de uma pessoa do sistema.
 * Recebe por padrão as informações do Bean, trata e passa para o DAO para persistência.
 * 
 * @author Ricardo Davanço Freire
 *
 */
@Stateless
public class PessoaListaBusiness {
	
	@Inject
	private PessoaListaDAO pessoaListaDao;

	/**
	 * Método chamado após a construção da tela para selecionar as pessoas cadastradas
	 * e exibir na interface.
	 * 
	 * @return Lista de pessoas para ser exibida na interface.
	 */
	public List<PessoaEntity> selecionaPessoas() {
		return pessoaListaDao.selecionaPessoas();
	}

	public String editaPessoa() {
		return "pessoa.xhtml";
	}

}
