package br.com.jexperts.business;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.primefaces.model.UploadedFile;

import br.com.jexperts.dao.PessoaDAO;
import br.com.jexperts.pojo.entity.PessoaEntity;

/**
 * 
 * Classe de business para as regras de negócio da tela de cadastro e edição de uma pessoa do sistema.
 * Recebe por padrão as informações do Bean, trata e passa para o DAO para persistência.
 * 
 * @author Ricardo Davanço Freire
 *
 */
@Stateless
public class PessoaBusiness {

	@Inject
	PessoaDAO pessoaDao;
	
	@Inject
	PessoaEntity pessoa;
	
	/**
	 *  Este método tem como finalidade salvar a foto no diretório <b>imagens/nome_da_foto</b>, 
	 *  gerar a miniatura no mesmo diretório com <b>nome_da_foto_min</b>, preencher os campos foto e miniaturaFoto na entidade e mandar
	 *  que o DAO faça a persistência.
	 *  
	 * @param pessoa - A entidade preenchida pelos campo no cadastro de pessoas
	 * @param foto - <code>UploadedFile</code> da foto da pessoa carregada na interface.
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 */
	public void salvar(PessoaEntity pessoa, UploadedFile foto) throws NoSuchAlgorithmException, IOException {
		
		pessoaDao.salvar(pessoa);
	}
	
	/**
	 * Método que faz a carga da foto selecionada pelo usuário na interface.
	 * Utiliza um InputStream e salva a foto no próprio projeto em um diretório chamado <b>imagens</b>.
	 * 
	 * @param nomeFoto - Nome da foto para salvar no arquivo de imagens.
	 * @param foto - InputStream da foto selecionada na interface.
	 * @return String com o nome da foto para adicionar no campo <b>foto</b> no banco de dados.
	 * @throws IOException
	 */
	public String carregaArquivo(String nomeFoto, InputStream foto) throws IOException{
		File arquivo = new File("imagens/" + nomeFoto);
		FileWriter fotoSalva = new FileWriter(arquivo);
		int lido = 0;
		while ((lido = foto.read()) != -1){
			fotoSalva.write(lido);
		}
		
		fotoSalva.close();
		pessoa.setFoto(arquivo.getName());
		return pessoa.getFoto();
	}

}
