package br.com.jexperts.view;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import br.com.jexperts.business.PessoaBusiness;
import br.com.jexperts.pojo.entity.PessoaEntity;

/**
 * 
 * Classe que recebe diretamente as chamadas do xhtml <b>pessoa.xhtml</b>.
 * Esta classe deve ser limpa, não conter regras de negócio, chamadas para o DAO e nem para banco de dados.
 * Para regras e outras execuções deve ser chamada a classe <b>PessoaBusiness</b>.
 * 
 * @author Ricardo Davanço Freire
 *
 */
@Named
@RequestScoped
public class PessoaBean {
	
	private static final String LISTA_PESSOA = "pessoaLista.xhml";

	@Inject
	PessoaEntity pessoa;
	
	@Inject
	PessoaBusiness pessoaBusiness;
	
	/**
	 * Foto adicionada no cadastro de pessoas.
	 */
	private UploadedFile foto;
	
	@PostConstruct
	public void init(){
		
	}
	
	/**
	 * Passa para o business o nome e o InputStream do arquivo selecionado pelo usuário.
	 * Método chamado ao selecionar a foto para carga, antes do salvamento da página.
 	 * 
	 * @throws IOException
	 */
	public void carregaArquivo() throws IOException {
		FacesMessage mensagem = new FacesMessage("Carregando foto.");
		FacesContext.getCurrentInstance().addMessage(null, mensagem);
		pessoaBusiness.carregaArquivo(foto.getFileName(), foto.getInputstream());
	}
	
	/**
	 * Método chamado ao clicar no botão salvar na tela de cadastro de pessoas.
	 * Apenas passa para o business a foto e a entidade Pessoa.
	 * 
	 * @return O arquivo de lista pessoaLista.xhtml
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 */
	public String salvar() throws NoSuchAlgorithmException, IOException{
		pessoaBusiness.salvar(pessoa, foto);
		return LISTA_PESSOA;
	}

	public PessoaEntity getPessoa() {
		return pessoa;
	}

	public void setPessoa(PessoaEntity pessoa) {
		this.pessoa = pessoa;
	}

	public UploadedFile getFoto() {
		return foto;
	}

	public void setFoto(UploadedFile foto) {
		this.foto = foto;
	}
	

}
