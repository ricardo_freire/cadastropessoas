package br.com.jexperts.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.jexperts.business.PessoaListaBusiness;
import br.com.jexperts.pojo.entity.PessoaEntity;


/**
 * 
 * Classe que recebe diretamente as chamadas do xhtml <b>pessoaLista.xhtml</b>.
 * Esta classe deve ser limpa, não conter regras de negócio, chamadas para o DAO e nem para banco de dados.
 * Para regras e outras execuções deve ser chamada a classe <b>PessoaListaBusiness</b>.
 * 
 * @author Ricardo Davanço Freire
 *
 */
@Named
@RequestScoped
public class PessoaListaBean {
	
	@Inject
	PessoaEntity pessoa;
	
	@Inject
	PessoaListaBusiness pessoaListaBusiness;
	
	private PessoaEntity pessoaSelecionada;
	
	private List<PessoaEntity> pessoas;
	
	@PostConstruct
	public void init(){
		pessoas = pessoaListaBusiness.selecionaPessoas();
	}
	
	public String editaPessoa(){
		return pessoaListaBusiness.editaPessoa();
	}

	public List<PessoaEntity> getPessoas() {
		return pessoas;
	}

	public void setPessoas(List<PessoaEntity> pessoas) {
		this.pessoas = pessoas;
	}

	public PessoaEntity getPessoaSelecionada() {
		return pessoaSelecionada;
	}

	public void setPessoaSelecionada(PessoaEntity pessoaSelecionada) {
		this.pessoaSelecionada = pessoaSelecionada;
	}
	
	
	
	

}
