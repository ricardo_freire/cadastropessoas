CREATE DATABASE jexperts;

USE jexperts;

CREATE TABLE pessoa (
	id_pessoa BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Identificador do usuario',
	nome_completo VARCHAR(150) NOT NULL COMMENT 'Nome completo do familiar',
	genero CHAR(1) NOT NULL COMMENT 'Genero da pessoa. M - Masculino, F - Feminino',
	data_nascimento DATE NOT NULL COMMENT 'Data de nascimento do familiar',
	cpf VARCHAR(14) NOT NULL COMMENT 'CPF do familiar',
	municipio VARCHAR(150) NOT NULL COMMENT 'Município de nascimento',
	uf CHAR(2) NOT NULL COMMENT 'Estado de nascimento',
	tel_celular VARCHAR(15) NOT NULL COMMENT 'Telefone celular',
	tel_fixo VARCHAR(15) NULL COMMENT 'Telefone Fixo',
	email VARCHAR(150) NULL COMMENT 'E-mail',
	foto VARCHAR(150) NULL COMMENT 'Caminho para a foto salva',
	miniatura_foto VARCHAR(150) NULL COMMENT 'Caminho para a miniatura da foto gerada pelo sistema',
	PRIMARY KEY (id_pessoa),
	UNIQUE (id_pessoa),
	UNIQUE (cpf)	
);

CREATE TABLE pessoa_parente (
	id_pessoa_parente BIGINT NOT NULL,
	id_pessoa BIGINT NOT NULL COMMENT 'Identificador do usuario que esta sendo cadastrado (id_usuario)',
	id_parente BIGINT NOT NULL COMMENT 'Identificador do parente relacionado (id_usuario)',
	grau_parentesco CHAR(1) NOT NULL COMMENT 'Grau de parentesco entre o usuario e seu parente. P - Pai, M - Mae, I - Irmao/Irma',
	PRIMARY KEY (id_pessoa_parente)
);
