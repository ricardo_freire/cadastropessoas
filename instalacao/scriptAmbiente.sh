#!/bin/bash

############################################################################
## 	Script de instalacao do ambiente de testes 			  ##  
## 	Este script tem por finalidade fazer as seguintes funcoes:        ##
## 	- Descompactar o arquivo de com o ambiente para teste.            ##
## 	- Instalar o mysql, caso nao exista.				  ##
## 	- Criar o banco de dados.					  ##
## 	- Mover a aplicacao para o diretorio de deploy do Wildfly      	  ##
## 	- Iniciar o Wildfly.						  ##
############################################################################

###################################### PARAMETROS ############################################
##											    ##
## Para o funcionamento correto deste script os seguintes parametros devem ser passados:    ##
##											    ##
## - WILDFLY -> Diretorio onde o Wildfly esta instalado.			            ##
##											    ##
##############################################################################################

## Parametro ##
WILDFLY=/development/wildfly

## Descompactando arquivo ##
tar -zxvf jexperts.tar.gz


## Instalacao do mysql para criacao do banco
sudo apt-get install mysql-server mysql-client


## Criacao do banco chamado jexperts a partir do dump no arquivo descompactado ##
mysql -u root -p < jexperts.sql

## Movendo arquivos para diretorio de deploy do Wildfly ##
cp -f standalone.xml ${WILDFLY}/standalone/configuration/
cp -rf CadastroPessoas.war ${WILDFLY}/standalone/deployments/
touch ${WILDFLY}/standalone/deployments/CadastroPessoas.war.dodeploy

## Inicio do servidor de aplicacao ##
mkdir traces
touch traces/standalone.log
chmod +x ${WILDFLY}/bin/standalone.sh
${WILDFLY}/bin/standalone.sh >> traces/standalone.log & 


## FIM ##
